#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for
from flask_cors import CORS
import os
import json
import configparser

# custom modules
import rssfeedparser

"""

pip requirements:
Flask
Flask-Cors
feedparser
beautifulsoup4


then to start run:
export FLASK_APP=main.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"

curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr



"""

############# reading configuration #############
config = configparser.ConfigParser()
config.read('config.ini') # TODO: kill app if this is not found




app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)


# comment/uncomment the desired services here
rssp = rssfeedparser.RSSParser()

@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"


##################### RSS Stuff #####################
@app.route('/rssfeedparser', methods=['GET'])
def rssfeedparser():

    if request.args.get('input') == None:
        return 'Please provide an RSS feed as input.\n'

    feedparserdict = rssp.parseFeed(request.args.get('input'))
    return json.dumps(feedparserdict)

@app.route('/collectContent', methods=['GET'])
def getTextContentFromFeed():

    if request.args.get('input') == None:
        return 'Please provide an RSS feed as input.\n'

    fpd = rssp.parseFeed(request.args.get('input'))
    
    for entry in fpd.get('entries'):
        txt = rssp.scrapeLink(entry.get('link'))
        entry['plaintext'] = txt
    return json.dumps(fpd)




    
if __name__ == '__main__':

    port = int(os.environ.get('PORT',5000))
    app.run(host='localhost', port=port, debug=True)
