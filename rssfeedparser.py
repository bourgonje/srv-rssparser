import feedparser
import urllib.request
from bs4 import BeautifulSoup


class RSSParser():

    def __init__(self):
        pass

    def parseFeed(self, feed):

        d = feedparser.parse(feed)
        return d
    
        
    def scrapeLink(self, link):

        html = urllib.request.urlopen(link).read()
        soup = BeautifulSoup(html)
        for script in soup(['script', 'style']):
            script.extract()
        text = soup.get_text()

        return text

